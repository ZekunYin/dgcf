#ifndef __ENCODETG__
#define __ENCODETG__

#include "fasta.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>
#include <cctype>
#include <algorithm>
#include <sstream>
#include <string>


void longTGRangeCheck(long curPos, long Max);


bool cmp_clusterunits(const hit &clusterunit0, const hit &clusterunit1);

void mergeTG(vector<GenomeClustInfo> &totalgenomes, long tgIndex, GenomeClustInfo &gi);


//encode TG and decode TG
long encodeTGtoMerge(long *&longTG, long longTG_Num, vector<GenomeClustInfo> &totalgenomes); //longTG_num -> max buffer length

void decodeLongTG_Merge(vector<GenomeClustInfo> &totalgenomes, long *&longTG, long longTG_Num); //longTG_num -> max buffer length

#endif
