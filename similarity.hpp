#ifndef __SIM__
#define __SIM__
#include "fasta.hpp"
#include <vector>
#include "paraSA.hpp"
#include "encodeSA.hpp"
enum algorithm{
	cdhit   ,
	gclust  ,
	mummer3 ,	
	esprit  ,
	dnaclust
};



void prepare_for_bcast(vector<Genome> &genomes,
					   vector<GenomeClustInfo> &totalgenomes,
					   long *&mpi_buffer, char *&str_buffer,
					   long &max_buffer_size, long &max_str_size,
					   long &buffer_len, long &str_len,
					   algorithm alg,
			  		   int *&mask_tg);


void prepare_for_cdhit();

void prepare_for_gclust(vector<Genome> &genomes, 
					   vector<GenomeClustInfo> &totalgenomes,
					   long *&mpi_buffer, char* &str_buffer,
					   long &max_buffer_size, long &max_str_size,
					   long &buffer_len, long &str_len,
					   int *&mask_tg);
void prepare_and_search(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id, algorithm alg);

void prepare_and_search_cdhit(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id);

void prepare_and_search_gclust(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id);

void mt_gclust_search(paraSA *&saa, vector<Genome> &refgenomes, 
					 vector<Genome> &qrygenomes,
					 vector<GenomeClustInfo> &totalgenomes,
					 int &chunk_start);
void mt_inner_gclust_search(paraSA *&saa, 
					 vector<Genome> &refgenomes, 
					 vector<GenomeClustInfo> &totalgenomes);
#endif //__SIM__
