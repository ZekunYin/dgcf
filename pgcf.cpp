//main to parallel greedy clustering framework for large scale biological sequences

#include "similarity.hpp"
#include "fasta.hpp"
#include "encodeTG.hpp"

#include <iostream>
#include <getopt.h>
#include <string>
#include <cctype>
#include <cstdlib>
#include <mpi.h>
#include <sstream>

using namespace std;

//global vars


//end global vars

//functions
void usage(string prog);
void init(long *&mpi_buffer, char * &str_buffer, 
		  long max_buffer_size,int *&mask_tg,
		  long max_str_size,long max_seq_num, algorithm alg);

void get_buffer_size(long max_chunk_size, long &max_buffer_size, 
						long &max_str_size, algorithm alg);

void outputClusteringInfoSimple(vector<GenomeClustInfo> &totalgenomes);
//end functions


int main(int argc, char* argv[]) 
{
	int threads = 1;
	bool vebose = false;
	bool loadall = false;
	int alg_int = 1;
	algorithm alg = gclust;

	vector<GenomeClustInfo> totalgenomes;//total genomes -- contains genome information
	vector<Genome> refgenomes;
	vector<Genome> genomes;
	vector<hit>  hit_info;
	vector<long> chunk_ptr;

	long total_chunks;
	long max_seq_num;
	long max_chunk_size;

	//vars for MPI
	int my_rank, rank_size;
	int worker_size;
	int worker_rank = -1;
	bool master = true;
	bool worker = false;
	int root = 0;
	int recv_tag;
	int collect_tag;
	//mpi buffer
	long *mpi_buffer;//if needed such as gclust
	char *str_buffer;
	long max_buffer_size = 0;
	long max_str_size = 0; 
	long buffer_len = 0; //real length of mpi buffer
	long str_len = 0; //real length of str buffer
	
	long len_buffer[2] = {0,0};	
	int *mask_tg;
	long *res_buffer;
	long res_buffer_size;
	//end vars for MPI


	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
	MPI_Comm_size(MPI_COMM_WORLD,&rank_size);

	worker_size = rank_size - 1; //assume one master node
	if(my_rank != 0){
		worker_rank = my_rank - 1;
		worker = true;
		master = false;
	}

	while (1) 
	{
		static struct option long_options[] =
		{ 
			{"threads",1,0,0}, // 0
			{"vebose",0,0,0} , // 1
			{"loadall",0,0,0}, // 2
			{"loadall",1,0,0}, // 3
			{0, 0, 0, 0}

		};


		int longindex = -1;
		int c = getopt_long_only(argc, argv, "", long_options, &longindex);
		if(c == -1) break; // Done parsing flags.
		else if(c == '?' && my_rank == 0){ // If the user entered junk, let him know. 
			cerr << "Invalid parameters." << endl;
			usage(argv[0]);
		}else {
			// Branch on long options.
			switch(longindex) 
			{ 
				case 0: threads = atoi(optarg); break;
				case 1: vebose = true		  ; break;
				case 2: loadall = true		  ; break;
				case 3: alg_int = atoi(optarg)       ; break;
				default: break; 
			}
		}

	}

	switch(alg_int)
	{
		case cdhit:    alg = cdhit       ; break;
		case gclust:   alg = gclust      ; break;
		case mummer3:  alg = mummer3     ; break;
		case esprit:   alg = esprit      ; break;
		case dnaclust: alg = dnaclust    ; break;
		default: cerr << "WARRING: Aalgorithm is not supported, default alg is used!" << endl; break;
	}


	if (argc - optind != 1 && my_rank == 0) usage(argv[0]);

	//get fasta file name
	string input_file = argv[optind];
	string res_file = input_file;
	string tg_file = input_file;


	res_file += ".out";

	tg_file += ".tg";
	if(vebose)
		cerr << " tg file  is " << tg_file << endl;

	if(worker){
		input_file +=".";	
		input_file += int2str(worker_rank);
	}

	if(vebose){
		cerr << "input file is " << input_file << "from rank " << my_rank << endl;
	}

	if(vebose && my_rank== 0)
		cerr << "result file is " << res_file  << endl;

	//read fasta 
	//read fasta info			
	load_total_genomes(tg_file, totalgenomes, total_chunks, max_seq_num, max_chunk_size,chunk_ptr);

	//init res buffer
	res_buffer_size = (long)totalgenomes.size() * 5;
	res_buffer = (long *)malloc(res_buffer_size * sizeof(long));

	if(master)
	for(int i = 0;i < chunk_ptr.size();i++)
		cerr << "chunk " << i << "start at " << chunk_ptr[i] << endl;	
		
	if(master){
		cerr << "max_seq_num is " << max_seq_num << endl;
		cerr << "max_chunk_size is " << max_chunk_size << endl;
	}

	if(worker){
		if(loadall)
			load_fasta_worker_all(input_file,genomes);
	}

	//check file consistancy
	if(worker && loadall){	
		for(int i = 0; i< genomes.size();i++)
			if(genomes[i].chunk_id != totalgenomes[genomes[i].id].chunk_id)
				cerr << "inconsistancy in seq " << genomes[i].id << endl;
		cerr << "check passed" << endl;	
	}

	//all are done chunk by chunk


	//alloc mem for mpi
	get_buffer_size(max_chunk_size,max_buffer_size,max_str_size,alg);	
	init(mpi_buffer,str_buffer,max_buffer_size,mask_tg,max_str_size,max_seq_num,alg);

	//framework start

	// fisrt step: make a ref db and query db		
	if(master){
		//load on chunk of fasta files //assume there is shared filesystem
		for(int i = 0; i < total_chunks - 1 ; i++){
			//chunk_id <-> i
			if(worker_size == 0){
				cerr << "no workers found" << endl;
				exit(0);
			}

			refgenomes.clear();
			load_one_chunk(input_file, refgenomes,i,worker_size);	
			//check refgenomes
			for(int j = 0; j< refgenomes.size();j++)
			if(refgenomes[j].size != totalgenomes[refgenomes[j].id].size)
				cerr << "inconsistancy in seq " << refgenomes[j].id << endl;
			cerr << "check passed round " << i << endl;	
			
			prepare_for_bcast(refgenomes,
							  totalgenomes,
						      mpi_buffer, str_buffer,
							  max_buffer_size, max_str_size,
							  buffer_len, str_len,
		  					  alg, mask_tg); //determain which search alg is used here

			len_buffer[0] = buffer_len;
			len_buffer[1] = str_len;
			MPI_Barrier(MPI_COMM_WORLD);
		
			//mpi bcast
			MPI_Bcast((void *)len_buffer, 2, MPI_LONG,
					   root, MPI_COMM_WORLD);
			MPI_Bcast((void *)mpi_buffer, (int) buffer_len, MPI_LONG,
    				  root, MPI_COMM_WORLD);
			MPI_Bcast((void *)str_buffer, (int) str_len, MPI_CHAR,
					  root, MPI_COMM_WORLD);
			//MPI_Barrier(MPI_COMM_WORLD);
//			//MPI_Recv(); // wait here util recv the mini-tg

			//recv mini mask tg from worker id
			int next_id = i + 1;
			int source = next_id % worker_size + 1; // rank 0 is master 
			MPI_Recv((void *)mask_tg, (int) max_seq_num, MPI_INT,source,recv_tag,MPI_COMM_WORLD, MPI_STATUS_IGNORE);//mpi recv mast_tg
			//copy mask_tg to totalgenomes
			for(int j = chunk_ptr[next_id] ;  j < chunk_ptr[next_id + 1] ;j++){
				if(mask_tg[j - chunk_ptr[next_id]] == 0)
					totalgenomes[j].rep = false;
			}
	
		}

		//deal with the last chunk
//		MPI_Recv((void *)mask_tg, (int) max_seq_num, MPI_INT,
//				(total_chunks-1 ) % worker_size,recv_tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);//mpi recv mast_tg
//
//		//copy mask_tg to totalgenoems
//		for(int j = chunk_ptr[total_chunks- 1] ;  j < chunk_ptr[total_chunks] ;j++){
//			if(mask_tg[j - chunk_ptr[total_chunks- 1]] == 0)
//				totalgenomes[j].rep = false;
//		}

		//collect result
		long res_num;
		for(int i = 0; i < worker_size; i++)
		{
			//MPI_Recv((void *)&res_num,1,MPI_LONG,i + 1,collect_tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			MPI_Recv((void *)res_buffer,(int) res_buffer_size,MPI_LONG,i + 1,collect_tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			decodeLongTG_Merge(totalgenomes,res_buffer,res_buffer_size);
		}
		
		//write out result	

		// Output with CD-HIT format.
		outputClusteringInfoSimple(totalgenomes); 
	}

	if(worker){
		for(int i = 0; i < total_chunks - 1 ;i++){
			if(loadall){
				MPI_Barrier(MPI_COMM_WORLD);

				MPI_Bcast((void *)len_buffer, 2, MPI_LONG,
					      root, MPI_COMM_WORLD);
				buffer_len = len_buffer[0];
				str_len = len_buffer[1];
				MPI_Bcast((void *)mpi_buffer, (int) buffer_len, MPI_LONG,
	    		          root, MPI_COMM_WORLD);
				MPI_Bcast((void *)str_buffer, (int) str_len, MPI_CHAR,
						  root, MPI_COMM_WORLD);
//				//MPI_Bcast()
				//MPI_Barrier(MPI_COMM_WORLD);
				cerr << "round " << i << " received" << endl;
				cerr << "receive buffer_len " << buffer_len << endl;
				cerr << "receive str_len " << str_len << endl;

				cerr << "start to search round " << i  << " from " << worker_rank<< endl;	
//				sleep(5);
				prepare_and_search(refgenomes,genomes,totalgenomes
								  ,mpi_buffer,
								  str_buffer, i,alg);
				
				//copy result to total genomes	
				int next_id = i + 1;
				if(worker_rank == (next_id % worker_size)){
					for(int j = chunk_ptr[next_id] ; j < chunk_ptr[next_id + 1] ;j ++)
						if(totalgenomes[j].rep)
							mask_tg[j - chunk_ptr[next_id]] = 1;
						else 
							mask_tg[j - chunk_ptr[next_id]] = 0;

					MPI_Send((void *) mask_tg,(int) max_seq_num, MPI_INT,root,recv_tag,MPI_COMM_WORLD);
				}
						


//				do_search(ref_genomes, genomes,alg);
//	
//				if(i % worker_size == worker_rank){
//					MPI_Send();//send mini-tg

//				}  

			}else{
				cerr << "not finished yet" << endl;
			}	
		}
//		//deal with last block
//		if(worker_rank == (total_chunks - 1 ) % worker_size){
//			for(int j = chunk_ptr[total_chunks - 1] ; j < chunk_ptr[total_chunks] ;j ++)
//				if(totalgenomes[j].rep)
//					mask_tg[j - chunk_ptr[total_chunks- 1]] = 1;
//				else 
//					mask_tg[j - chunk_ptr[total_chunks- 1]] = 0;
//			}
//						
//		MPI_Send((void *) mask_tg,(int) max_seq_num, MPI_INT,root,recv_tag,MPI_COMM_WORLD);

		encodeTGtoMerge(res_buffer,res_buffer_size,totalgenomes);
		MPI_Send((void *)res_buffer,(int)res_buffer_size,MPI_LONG, root,collect_tag,MPI_COMM_WORLD);	
	}

	
	MPI_Finalize();
	return 0;
}

void usage(string prog) 
{
	cerr << "Parallel Greedy Clustering Framework v0.1" << endl;
	exit(1);

}

void init(long *&mpi_buffer, char * &str_buffer, 
		  long max_buffer_size,int *&mask_tg,
		  long max_str_size,long max_seq_num, algorithm alg){

	cerr << "init parallel greedy clustering framework" << endl;
	if(alg == gclust){
		mpi_buffer = (long *)malloc(max_buffer_size * sizeof(long));
		for(int i = 0;i < max_buffer_size; i++)
			mpi_buffer[i] = (long) 0;
	}

	str_buffer = (char *) malloc(max_str_size * sizeof(char));
	mask_tg = (int *) malloc(max_seq_num * sizeof(int));

	for(int i = 0;i < max_seq_num; i++)
		mask_tg[i] = 1;
	
			
	for(int i = 0;i < max_str_size; i++)
		str_buffer[i] = '\0';
	cerr << "mask_tg size is " << max_seq_num << endl;
	cerr << "total mpi_buffer memory consumption " << max_buffer_size * sizeof(long) + max_str_size * sizeof(char) << endl; 
	
}

void get_buffer_size(long max_chunk_size, long &max_buffer_size, 
						long &max_str_size, algorithm alg)
{
	if(alg == gclust)
		max_buffer_size = max_chunk_size *  1;//need tobe modified
		
	max_str_size = max_chunk_size;
}
	// Note: output clustering information as cd-hit format.
void outputClusteringInfoSimple(vector<GenomeClustInfo> &totalgenomes)
{
	long clusters = 0;
	for (long i=0; i<(long)totalgenomes.size(); i++)
	{	
		if (totalgenomes[i].rep)
		{
			long clusterunit=0;
			// Output representive.
			cout<<">Cluster "<<clusters<<endl;
			cout<<clusterunit<<"\t"<<totalgenomes[i].size<< \
				"nt, >"<<totalgenomes[i].descript<< \
				"... *"<<endl;
			if (totalgenomes[i].clusterunits.size()>0) 
			{
				for (long j=0;j<(long)totalgenomes[i].clusterunits.size();j++) 
				{
					clusterunit++;
					cout<<clusterunit<<"\t"<< \
						totalgenomes[totalgenomes[i].clusterunits[j].id].size<<"nt, >"<< \
						totalgenomes[totalgenomes[i].clusterunits[j].id].descript \
						<<"... at "<<totalgenomes[i].clusterunits[j].strand<<"/"<< \
						totalgenomes[i].clusterunits[j].identity<<endl;
				}
			}
			clusters++;
		}
	}
	cerr<<"Total clusters: "<<clusters<<endl;

}
