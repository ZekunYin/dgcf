CC = icc
CXX = icpc
MPICC = mpiicc
MPICXX = mpiicpc

FLAGS = -I ./ -O3 -g -qopenmp
SRC = pgcf.cpp fasta.cpp similarity.cpp paraSA.cpp encodeSA.cpp encodeTG.cpp

all: pgcf

pgcf: pgcf.o fasta.o similarity.o paraSA.o encodeSA.o encodeTG.o
	$(MPICXX)   $(FLAGS) $^ -o $@ 

.cpp.o:
	$(MPICXX)   $(FLAGS) -Wall -c $<

.c.o:
	$(MPICC) $(FLAGS) -Wall -c $<

clean: 
	rm -f *.o pgcf

