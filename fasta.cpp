#include <fstream>
#include <iostream>
#include <algorithm>
#include "fasta.hpp"
#include <sstream>
#include <algorithm>

using namespace std;


string int2str(int a){
	ostringstream os;
	os << a;
	return os.str();
}
// Filter 'n' in genome.
void filter_n(string &seq_rc) 
{
	// Bit by bit.
  for(long i = 0; i < (long)seq_rc.length(); i++) {
    // Adapted from Kurtz code in MUMmer v3. 
    switch(seq_rc[i]) 
		{
			case 'a': case 't': case 'g': case 'c': break;
			default:
				cerr << "ERR " << seq_rc[i] << " not recognized" << endl;
				//seq_rc[i] = '~';
		}
  }
}

// Return the reverse complement of sequence. This allows searching
// the plus strand of instances on the minus strand.
void reverse_complement(string &seq_rc, bool nucleotides_only) 
{
  // Reverse in-place.
  reverse(seq_rc.begin(), seq_rc.end());
  for(long i = 0; i < (long)seq_rc.length(); i++) 
	{
    // Adapted from Kurtz code in MUMmer v3. 
    switch(seq_rc[i]) 
		{
    case 'a': seq_rc[i] = 't'; break;
    case 'c': seq_rc[i] = 'g'; break;
    case 'g': seq_rc[i] = 'c'; break;
    case 't': seq_rc[i] = 'a'; break;
    case 'r': seq_rc[i] = 'y'; break; /* a or g */
    case 'y': seq_rc[i] = 'r'; break; /* c or t */
    case 's': seq_rc[i] = 's'; break; /* c or g */
    case 'w': seq_rc[i] = 'w'; break; /* a or t */
    case 'm': seq_rc[i] = 'k'; break; /* a or c */
    case 'k': seq_rc[i] = 'm'; break; /* g or t */
    case 'b': seq_rc[i] = 'v'; break; /* c, g or t */
    case 'd': seq_rc[i] = 'h'; break; /* a, g or t */
    case 'h': seq_rc[i] = 'd'; break; /* a, c or t */
    case 'v': seq_rc[i] = 'b'; break; /* a, c or g */
    default:  
      if(!nucleotides_only) seq_rc[i] = 'n'; 
      break; /* anything */
    }
  }

}


// trim a string.
void trim(string &line, long &start, long &end) 
{
	// Trim leading spaces. 
	for(long i = start; i < (int)line.length(); i++) 
	{ 
		if(line[i] != ' ') 
		{ 
			start = i; 
			break;
		} 
	}
	// Trim trailing spaces.
	for(long i = line.length() - 1; i >= 0; i--) { 
		if(line[i] != ' ') { 
			end = i;
			break;
		} 
		if(i == 0) break;
	}
}

//Note  only used for gclust move it to paraSA.cpp
// Make one suffix array from one block.
void make_block_ref(vector<Genome> &partgenomes, 
		vector<GenomeClustInfo> &totalgenomes,
		string &S,
		int *&mask_tg,
		vector<long> &descr, 
		vector<long> &startpos)
{
	long pos = 0;
	Genome tg;
	long s=partgenomes.size();
	S="";
	startpos.push_back(0);
	for (long i=0;i<s;i++){	
		tg=partgenomes[i];
		if (!totalgenomes[tg.id].rep){
		//if(!mask_tg[i]){
			continue;
		}
		S += tg.cont;
		S += '`';
		pos = pos+tg.size+1;
		startpos.push_back(pos);
		descr.push_back(tg.id);
	}
	startpos.pop_back();
	int k = S.length(); 
	S = S.substr(0,k-1);
	cerr<<"\n===="<<endl;
	cerr<<"S "<<S.length()<<endl;
	cerr<<"startpos "<<startpos.size()<<endl;
	cerr<<"descr "<<descr.size()<<endl;
	cerr<<"====\n"<<endl;

}

// Load total genomes one time for mpi base version
void load_total_genomes(string filename, 
		vector<GenomeClustInfo> &totalgenomes,
		long &total_chunks,
		long &max_seq_num,
		long &max_chunk_size,
		vector<long> &chunk_ptr)
{
	total_chunks = 0;
	long length, maxlen, totallen, minlen;
	length=maxlen=totallen=0;
	//minlen=MAX_GENOME;
	long id, loadnum; // Genome id from previous.
	long chunk_id;
	id=loadnum=0;
	string meta, line;
	GenomeClustInfo tg;
	stringstream buffer;	
	long last_chunk_id = -1;
	// Everything starts at zero.
	ifstream data(filename.c_str());
	if(!data.is_open()) { 
		cerr << "unable to open " << filename << endl; 
		exit(1); 
	}

	getline(data,line);//get the first line for max seqnums and chunk size
	buffer.str("");
	buffer << line;
	buffer >> max_chunk_size;
	buffer >> max_seq_num;


	while(data.peek() != EOF) {
		getline(data, line); // Load one line at a time.
		//trim(line, start, end);
		buffer.clear();
		buffer.str("");
		buffer << line;
		buffer >> tg.chunk_id;
		buffer >> tg.id;
		buffer >> tg.size;
		buffer >> tg.descript;
		tg.rep = true;
		chunk_id = tg.chunk_id;	
		if(chunk_id != last_chunk_id)
		{
			total_chunks++;
			chunk_ptr.push_back((long)totalgenomes.size());
			last_chunk_id = chunk_id;
		}
		totallen += tg.size;
		totalgenomes.push_back(tg);	
		loadnum++;

	}
	
	chunk_ptr.push_back((long)totalgenomes.size());

	cerr<<"Genomes: "<<loadnum<<endl;
	cerr<<"Total chunks: "<<total_chunks<<endl;
	cerr<<"Maximum Length: "<<totalgenomes[0].size<<endl;
	cerr<<"Minimum Length: "<<totalgenomes[loadnum - 1].size<<endl;
	cerr<<"Average Length: "<<totallen/loadnum<<endl;
	cerr<<"\n";
	cerr<<"=====================\n\n";

}


void load_fasta_worker_all(string filename, vector<Genome> &allgenomes){
	long loadnum = 0;
	long count = 0;
	string line;
	string cont;
	string meta;
	Genome genome;
	stringstream buffer;

	ifstream data(filename.c_str());

	if(!data.is_open()){
		cerr << "unable to open " << filename << endl;
		exit(1);
	}

	while(data.peek() != EOF)	
	{
		getline(data,line);
		//read file until to get the first >
		if(line[0] == '>'){
			meta = line;
			break;
		}
		count ++;
	}
	cerr << count << " lines jumped" << endl; 


	//start to read file 
	while(data.peek() != EOF){
		getline(data,line);
		if(line[0] == '>'){
			buffer.clear();
			buffer.str("");
			buffer << meta;
			buffer >> genome.descript;		
			buffer >> genome.id;
			buffer >> genome.chunk_id;
			transform(cont.begin(), cont.end(), cont.begin(), ::tolower);
			genome.cont = cont;
			genome.size = cont.size();
			allgenomes.push_back(genome);
			loadnum ++;
			meta = line;
			cont = "";		
		}
		else{
			cont += line;
		}		
	}	

	//deal with the last sequences
	buffer.clear();
	buffer.str("");
	buffer << meta;
	buffer >> genome.descript;		
	buffer >> genome.id;
	buffer >> genome.chunk_id;
	transform(cont.begin(), cont.end(), cont.begin(), ::tolower);
	genome.cont = cont;
	genome.size = cont.size();
	allgenomes.push_back(genome);
	loadnum ++;

	cerr << "Loadall " << loadnum << "sequences" << endl;	

	return;

}


void load_one_chunk(string filename, vector<Genome> &chunk_genomes,
		long chunk_id, int worker_size)
{
	long count = 0;
	long loadnum = 0;
	string line;
	string cont;
	string meta;
	Genome genome;
	stringstream buffer;

	//get file name
	filename += ".";			
	if(worker_size == 0)
		filename += '0';
	else
		filename += int2str(chunk_id % worker_size);

	ifstream data(filename.c_str());

	if(!data.is_open()){
		cerr << "unable to open " << filename << endl;
		exit(1);
	}

	//	while(data.peek() != EOF)	
	//	{
	//		getline(data,line);
	//		//read file until to get the first >
	//		if(line[0] == '>'){
	//			meta = line;
	//			break;
	//		}
	//		count ++;
	//	}
	//	cerr << count << " lines jumped" << endl; 

	count = 0;
	while(data.peek()!= EOF)
	{
		getline(data,line);
		if(line[0] == '>')
		{
			buffer.clear();
			buffer.str("");
			buffer << line;
			buffer >> genome.descript;
			buffer >> genome.id;
			buffer >> genome.chunk_id;
			if(genome.chunk_id == chunk_id)
			{   
				meta  = line;
				break;
			}
			count ++;
		}else{
			continue;
		}
	}

	cerr << "chunk " << chunk_id << " ";
	cerr << count << " sequences jumped" << endl;
	//start to read file 
	while(data.peek() != EOF){
		getline(data,line);
		if(line[0] == '>'){
			buffer.clear();
			buffer.str("");
			buffer << meta;
			buffer >> genome.descript;		
			buffer >> genome.id;
			buffer >> genome.chunk_id;

			if(genome.chunk_id != chunk_id) break;

			transform(cont.begin(), cont.end(), cont.begin(), ::tolower);
			genome.cont = cont;
			genome.size = cont.size();
			chunk_genomes.push_back(genome);
			loadnum ++;
			meta = line;
			cont = "";		
		}
		else{
			cont += line;
		}		
	}	

	//deal with the last seq
	if(data.peek() == EOF){
		buffer.clear();
		buffer.str("");
		buffer << meta;
		buffer >> genome.descript;		
		buffer >> genome.id;
		buffer >> genome.chunk_id;

		if(genome.chunk_id == chunk_id){

			transform(cont.begin(), cont.end(), cont.begin(), ::tolower);
			genome.cont = cont;
			genome.size = cont.size();
			chunk_genomes.push_back(genome);
			loadnum ++;
		}
	}
	cerr << "chunk " << chunk_id << " " << loadnum << " loaded " << endl;

	return ;

}


// Note: collect clustering information.
void getClusteringInfoOnepart(vector<GenomeClustInfo> &totalgenomes,
		vector<Genome> &qrygenomes,
		long begin,
		long chunk,
		bool &clusterhit)
{	
	long b,e;
	clusterhit=false;
	b=begin;
	e=begin+chunk;
	for (long i=b; i<e; i++)
	{	
		int id = qrygenomes[i].id;
		if (!totalgenomes[id].rep){ continue; }	
		if (totalgenomes[id].clusters.size()>0)
		{	
			for (long j=0; j<(long)totalgenomes[id].clusters.size(); j++)
			{
				hit thit;
				thit=totalgenomes[id].clusters[j];
				if (totalgenomes[thit.id].rep)
				{
					hit thit0;
					thit0.id=id;
					thit0.identity=thit.identity;
					thit0.strand=thit.strand;
					totalgenomes[thit.id].clusterunits.push_back(thit0);
					totalgenomes[id].rep=false;
					clusterhit=true;
					break;
				}
			}
			// Note: important clear
			totalgenomes[id].clusters.clear();
		}
	}
}










