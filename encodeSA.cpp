#include <cstdlib>
#include "encodeSA.hpp"
#include <cstdio>
#include <cstring>
void printfSAString(paraSA *&saa) {

	if((saa->S).length()==0) {
		printf("saa->S.length == 0\n");
	}
	else {
		//printf("PRINTF S:\n%s\n\n", (saa->S).c_str());
		printf("PRINTF S:\n");
		for(long i=0; i<(long)(saa->S).length(); i++){
			if(i%300000==0) printf("\n");
			if(i%30000==0) printf("%c", (saa->S)[i]);
		}
		printf("END");
	}

}

void deleteSAString(paraSA *&saa){

	for(long i=0; i<(long)(saa->S).length(); i++){
		(saa->S)[i] = 'a';
	}
	saa->S = "";
}

void encodeSAString(char *&charbuff, long &rlen, paraSA *&saa) {

	if(charbuff==NULL || saa==NULL) {
		cerr<<"Bug: encodeSAString: pointer charbuff = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}
	if((saa->S).length()==0) {
		cerr<<"Bug: encodeSAString: saa->S.length == 0"<<endl;
		exit(1);
	}

	//const char *str = (saa->S).c_str();
	long realLen = (saa->S).length();
	strcpy(charbuff,(saa->S).c_str());//assume char buffer size is larger than s.length
	//charbuff = str;
	rlen = realLen;
}

void decodeSAString(paraSA *&saa, vector<Genome> &ref_genome, char *&charbuff) {

	if(charbuff==NULL || saa==NULL) {
		cerr<<"Bug: decodeSAString: pointer charbuff = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}
	if((saa->S).length()!=0) {
		cerr<<"Warning: decodeSAString: saa->S.length == 0"<<endl;
		saa->S = "";
	}
	if(ref_genome.size()!=0) {
		cerr<<"Warning: decodeSAString: vector<Genome> ref_genome.length == 0"<<endl;
		ref_genome.clear();
	}

	saa->S = charbuff;

	// decode saa->S to vector<Genome> ref_genome
	string ref_string = charbuff;
	ref_string += "'";// append the last char to add the last index, for function sub_str()

	Genome gm;
	string g_cont ="";
	vector<long> &pos = saa->startpos;
	vector<long> &ref_id = saa->descr;

	if( pos.size()!=0 && pos.size()==ref_id.size() ) {
		//printf("begin decode string to  refseqs.\n");
		for(long i=0; i<(long)pos.size(); i++) {

			long start = pos[i];
			long end = pos[i+1] - 1;
			if(i == (long)pos.size()-1) {
				end = (long)ref_string.size() - 1;
				for(long j=start; j<(long)ref_string.size(); j++) {
					if(ref_string[j] == '$') {
						end = j;
						break;
					}

				}
			}

			gm.id = ref_id[i];
			gm.size = end - start;
			gm.cont = ref_string.substr(start, end - start);

			ref_genome.push_back(gm);
			if(i!=(long)pos.size()-1) {
				for(long j=ref_id[i]+1; j<ref_id[i+1]; j++) {
					gm.id = j;
					gm.size = 0;
					gm.cont = "";
					ref_genome.push_back(gm);
				}
			}

		}
	}
	else {
		cerr<<"Error: decodeSAString: decode charbuff to vector<Genome> ref_genome error."<<endl;
		exit(1);
	}

}

void printfSA(paraSA *&saa, long descr_len, long startpos_len, long sa_len, long isa_len, long lcp_vec_len, long lcp_M_len)
{

	printf("printfSA Delete: sa_len = %ld, isa_len = %ld, lcp_vec_len = %ld, lcp_M_len = %ld\n", sa_len, isa_len , lcp_vec_len, lcp_M_len);
	printf("N = %ld, K = %ld, logN = %ld, NKm1 = %ld\n", saa->N, saa->K, saa->logN, saa->NKm1);

	// descr && startpos
	//long descr_len = (long)(saa->descr).size();
	//long startpos_len = (long)(saa->startpos).size();

	printf("descr_len = %ld, startpos_len = %ld\n", descr_len, startpos_len);
	if(descr_len == startpos_len) {
		printf("saa->descr = %ld\n", descr_len);
		for(long i=0; i<descr_len; i++)
		{
			printf("%ld ", (saa->descr)[i]);
		}
		printf("\n");
		printf("saa->startpos = %ld\n", startpos_len);
		for(long i=0; i<startpos_len; i++)
		{
			printf("%ld ", (saa->startpos)[i]);
		}
		printf("\n");
	}
	else {
		printf("printf saa, descr_len != startpos_len\n");
		exit(1);
	}

	// SA && ISA && LCP
	//long sa_len = (long)(saa->SA).size();
	//long isa_len = (long)(saa->ISA).size();
	//long lcp_vec_len = (long)(saa->LCP).vec.size();
	//long lcp_M_len = (long)(saa->LCP).M.size();

	printf("sa_len = %ld, isa_len = %ld\n", sa_len, isa_len);
	if(sa_len == isa_len) {
		printf("saa->SA = %ld\n", sa_len);
		for(long i=0; i<sa_len; i++)
		{
			if(i%3000000==0) printf("\n");
			if(i%300000==0) printf("%d ", (saa->SA)[i]);
		}
		printf("\n");

		printf("saa->ISA = %ld\n", isa_len);
		for(long i=0; i<isa_len; i++)
		{
			if(i%3000000==0) printf("\n");
			if(i%300000==0) printf("%d ", (saa->ISA)[i]);
		}
		printf("\n");

	}
	else {
		printf("printf saa, sa_len != isa_len\n");
		exit(1);
	}

	printf("lcp_vec_len = %ld, lcp_M_len = %ld\n", lcp_vec_len, lcp_M_len);
	if(sa_len == lcp_vec_len) {
		printf("saa->LCP\n");

		printf("saa->LCP.vec = %ld\n", lcp_vec_len);
		for(long j=0; j<lcp_vec_len; j++) {
			if(j%300000==0) printf("\n");
			if(j%30000==0) printf("%u ", (saa->LCP).vec[j]);
		}
		printf("\n");

		printf("saa->LCP.M = %ld\n", lcp_M_len);
		for(long j=0; j<lcp_M_len; j++) {
			if(j%20000==0) printf("\n");
			if(j%2000==0) printf("%zu,%d   ", (saa->LCP).M[j].idx, (saa->LCP).M[j].val);
		}
		printf("\n");

	}
	else {
		printf("printf saa, sa_len != lcp_vec_len\n");
		exit(1);
	}

}

void printfSA(paraSA *&saa)
{
	printf("N = %ld, K = %ld, logN = %ld, NKm1 = %ld\n", saa->N, saa->K, saa->logN, saa->NKm1);

	// descr && startpos
	long descr_len = (long)(saa->descr).size();
	long startpos_len = (long)(saa->startpos).size();

	printf("descr_len = %ld, startpos_len = %ld\n", descr_len, startpos_len);
	if(descr_len == startpos_len) {
		printf("saa->descr = %ld\n", descr_len);
		for(long i=0; i<descr_len; i++)
		{
			printf("%ld ", (saa->descr)[i]);
		}
		printf("\n");
		printf("saa->startpos = %ld\n", startpos_len);
		for(long i=0; i<startpos_len; i++)
		{
			printf("%ld ", (saa->startpos)[i]);
		}
		printf("\n");
	}
	else {
		printf("printf saa, descr_len != startpos_len\n");
		exit(1);
	}
/*
	// SA && ISA && LCP
	long sa_len = (long)(saa->SA).size();
	long isa_len = (long)(saa->ISA).size();
	long lcp_vec_len = (long)(saa->LCP).vec.size();
	long lcp_M_len = (long)(saa->LCP).M.size();

	printf("sa_len = %ld, isa_len = %ld\n", sa_len, isa_len);
	if(sa_len == isa_len) {
		printf("saa->SA = %ld\n", sa_len);
		for(long i=0; i<sa_len; i++)
		{
			if(i%150000==0) printf("\n");
			if(i%15000==0) printf("%d ", (saa->SA)[i]);
		}
		printf("\n");

		printf("saa->ISA = %ld\n", isa_len);
		for(long i=0; i<isa_len; i++)
		{
			if(i%150000==0) printf("\n");
			if(i%15000==0) printf("%d ", (saa->ISA)[i]);
		}
		printf("\n");

	}
	else {
		printf("printf saa, sa_len != isa_len\n");
		exit(1);
	}

	printf("lcp_vec_len = %ld, lcp_M_len = %ld\n", lcp_vec_len, lcp_M_len);
	if(sa_len == lcp_vec_len) {
		printf("saa->LCP\n");

		printf("saa->LCP.vec = %ld\n", lcp_vec_len);
		for(long j=0; j<lcp_vec_len; j++) {
			if(j%150000==0) printf("\n");
			if(j%15000==0) printf("%u ", (saa->LCP).vec[j]);
		}
		printf("\n");

		printf("saa->LCP.M = %ld\n", lcp_M_len);
		for(long j=0; j<lcp_M_len; j++) {
			if(j%15000==0) printf("\n");
			if(j%1500==0) printf("%zu,%d   ", (saa->LCP).M[j].idx, (saa->LCP).M[j].val);
		}
		printf("\n");

	}
	else {
		printf("printf saa, sa_len != lcp_vec_len\n");
		exit(1);
	}
*/
}

void deleteSA(paraSA *&saa)
{
	saa->N = 0;
	saa->K = 0;
	saa->logN = 0;
	saa->NKm1 = 0;

	// descr && startpos
	long descr_len = (long)(saa->descr).size();
	long startpos_len = (long)(saa->startpos).size();

	if(descr_len == startpos_len) {
		printf("deleteSA: descr_len == startpos_len = %ld\n", descr_len);
		for(long i=0; i<descr_len; i++)
		{
			(saa->descr)[i] = 0;
			(saa->startpos)[i] = 1;
		}
	}
	else {
		printf("deleteSA: descr_len != startpos_len\n");
		exit(1);
	}
	(saa->descr).clear();
	(saa->startpos).clear();


	// SA && ISA && LCP
	long sa_len = (long)(saa->SA).size();
	long isa_len = (long)(saa->ISA).size();
	long lcp_vec_len = (saa->LCP).vec.size();
	long lcp_M_len = (saa->LCP).M.size();

	if(sa_len == isa_len) {
		printf("deleteSA: sa_len == isa_len = %ld\n", sa_len);
		for(long i=0; i<sa_len; i++)
		{
			(saa->SA)[i] = 2;
			(saa->ISA)[i] = 3;
		}
	}
	else {
		printf("deleteSA: sa_len != isa_len\n");
		exit(1);
	}
	(saa->SA).clear();
	(saa->ISA).clear();

	if(sa_len == lcp_vec_len) {

		printf("deleteSA: lcp_vec_len = %ld, lcp_M_len = %ld\n", lcp_vec_len, lcp_M_len);

		if(lcp_vec_len!=0) {
			for(long j=0; j<lcp_vec_len; j++) {
				(saa->LCP).vec[j] = 67;
			}
		}

		if(lcp_M_len!=0) {
			for(long j=0; j<lcp_M_len; j++) {
				(saa->LCP).M[j].idx = 4;
				(saa->LCP).M[j].val = 5;
			}
		}
		if(lcp_vec_len==0 || lcp_M_len==0) {
			printf("deleteSA: lcp_vec_len = %ld, lcp_M_len = %ld\n", lcp_vec_len, lcp_M_len);
			exit(1);
		}

	}
	else {
		printf("deleteSA: sa_len != lcp_vec_len\n");
		exit(1);
	}
	(saa->LCP).vec.clear();
	(saa->LCP).M.clear();

}

void encodeSAPart(long *&buffer, long _BufferSize, paraSA *&saa, long &rlen)
{

	if(buffer == NULL || saa ==NULL) {
		cerr<<"Bug: encodeSAPart: pointer charbuff = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}

	long BufferSize = _BufferSize;
	long realLen = 0;
	long realstart = 0;

	// N, K, logN, NKm1: len = 4
	long descr_len = (long)(saa->descr).size();
	long startpos_len = (long)(saa->startpos).size();

	long sa_len = (long)(saa->SA).size();
	long isa_len = (long)(saa->ISA).size();

	long lcp_vec_len = (long)(saa->LCP).vec.size();
	long lcp_M_len = (long)(saa->LCP).M.size();

	long length = 4 + descr_len + startpos_len + sa_len + isa_len + lcp_vec_len + lcp_M_len*2 + 4;
//printf("Encode Length set : descr_len = %ld, startpos_len = %ld, sa_len = %ld, isa_len = %ld, lcp_vec_len = %ld, lcp_M_len = %ld\n\n", descr_len, startpos_len, sa_len, isa_len, lcp_vec_len, lcp_M_len);
	if(descr_len==0 || startpos_len==0 || sa_len==0 || isa_len==0 || lcp_vec_len==0 || lcp_M_len==0) {
		cerr<<"Bug: encodeSAPart: (saa->descr).size == 0 || (saa->startpos).size == 0 || (saa->SA).size == 0 || (saa->ISA).size == 0 || (saa->LCP).vec.size == 0 || (saa->LCP).M.size == 0"<<endl;  
		exit(1);
	}

	if(length<BufferSize) {

		// encode N, K, logN, NKm1
		//printf("encode N, K, logN, NKm1\n...\n");
		buffer[0] = saa->N;
		buffer[1] = saa->K;
		buffer[2] = saa->logN;
		buffer[3] = saa->NKm1;
		realstart = realLen;
		realLen += 4;

		// encode descr, startpos
		if(descr_len == startpos_len) {
			//printf("encode descr, startpos\n...\n");
			long *pos_descr;
			pos_descr = buffer+realLen;

			long encode_len = startpos_len*2;// encode two elements in one loop
			pos_descr[0] = encode_len;//startpos_len*2;
			for(long i=1, j=1; i<=encode_len/2; i++, j+=2) {
				pos_descr[j] = saa->descr[i-1];
				pos_descr[j+1] = saa->startpos[i-1];
			}
			realstart = realLen;
			realLen += (encode_len+1);

		}
		else {
			cerr<<"Bug: encodeSAPart: descr_len != startpos_len"<<endl;
			exit(1);
		}


		// encode SA, ISA
		if(sa_len == isa_len) {
			//printf("encode SA, ISA\n...\n");
			long *sa_isa;
			sa_isa = buffer+realLen;

			long encode_len = sa_len*2;// encodetwo elements in one loop
			sa_isa[0] = encode_len;//sa_len*2;
			for(long i=1, j=1; i<=encode_len/2; i++, j+=2) {
				sa_isa[j] = saa->SA[i-1];
				sa_isa[j+1] = saa->ISA[i-1];
			}
			realstart = realLen;
			realLen += (encode_len+1);

		}
		else {
			cerr<<"Bug: encodeSAPart: sa_len != isa_len"<<endl;
			exit(1);
		}


		// encode LCP.vec
		if(sa_len == lcp_vec_len) {
			//printf("encode LCP.vec\n...\n");
			long *lcp_vec;
			lcp_vec = buffer+realLen;

			long encode_len = lcp_vec_len;// encode one element in one loop
			lcp_vec[0] = encode_len;//lcp_vec_len;
			for(long i=1; i<=encode_len; i++) {
				lcp_vec[i] = (long)(saa->LCP).vec[i-1];
			}
			realstart = realLen;
			realLen += (encode_len+1);

		}
		else {
			cerr<<"Bug in encodeSAPart: sa_len != lcp_vec_len"<<endl;
			exit(1);
		}


		// encode LCP.M
		//printf("encode LCP.M\n...\n");
		long *lcp_M;
		lcp_M = buffer+realLen; 

		long encode_len = lcp_M_len*2;// encode two elements in one loop
		lcp_M[0] = encode_len;//lcp_M_len*2;
		for(long i=1, j=1; i<=encode_len/2; i++, j+=2) {
			lcp_M[j] = (long)(saa->LCP).M[i-1].idx;
			lcp_M[j+1] = (long)(saa->LCP).M[i-1].val;
		}
		realstart = realLen;
		realLen += (encode_len+1);

		/* the size_t is defined as unsigned int or unsigned long depending on system, so steps below are useless */
		// the size of type size_t is not equal with the size of type long
		//long offset = (long)(sizeof(size_t)/sizeof(long));
		//printf("encodeSAPart: sizeof(size_t) = %ld, sizeof(long) = %ld, offset = %ld\n", sizeof(size_t), sizeof(long), offset);
		//long encode_len = lcp_M_len*(offset+1);// encode two elements in one loop
		//lcp_M[0] = encode_len;//lcp_M_len*2;
		//for(long i=1, j=1; i<=encode_len/(offset+1); i++, j+=offset+1) {

		//	size_t idx = (saa->LCP).M[i-1].idx;
		//	long *idx_p;
		//	idx_p = (long*)&idx;

		//	for(long k=0; k<offset; k++) {
		//		lcp_M[j+k] = (*idx_p);
		//		idx_p++;
		//	}

		//	lcp_M[j+offset] = (long)(saa->LCP).M[i-1].val;
		//}
		//realstart = realLen;
		//realLen += (encode_len+1);

	}
	else {
		cerr<<"index overflow, range out of buffer size."<<endl;
		exit(1);
	}

	rlen = realLen;

	cerr<<"BufferSize = "<<BufferSize<<endl;
	cerr<<"length     = "<<length<<endl;
	cerr<<"realLen    = "<<rlen<<endl;

}

void encodeSA(long *&buffer, char *&charbuff, long &rlen, long &crlen, long _BufferSize, paraSA *&saa)
{

	if(charbuff==NULL || buffer == NULL || saa ==NULL) {
		cerr<<"Bug: encodeSA: pointer buffer = NULL or charbuff = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}
	if((saa->S).length()==0) {
		cerr<<"Bug: encodeSA: saa->S.length == 0 for encodeSAString function."<<endl;
		exit(1);
	}

	encodeSAPart(buffer, _BufferSize, saa, rlen);
	encodeSAString(charbuff, crlen, saa);

}

void decodeSAPart(paraSA *&saa, long *&buffer)
{

	if(buffer == NULL || saa ==NULL) {
		cerr<<"Bug: decodeSAPart: pointer charbuff = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}

	long realLen = 0;
	long realstart = 0;


	long descr_len = (long)(saa->descr).size();
	long startpos_len = (long)(saa->startpos).size();

	long sa_len = (long)(saa->SA).size();
	long isa_len = (long)(saa->ISA).size();

	long lcp_vec_len = (long)(saa->LCP).vec.size();
	long lcp_M_len = (long)(saa->LCP).M.size();

//printf("Decode Length set: descr_len = %ld, startpos_len = %ld, sa_len = %ld, isa_len = %ld, lcp_vec_len = %ld, lcp_M_len = %ld\n\n", descr_len, startpos_len, sa_len, isa_len, lcp_vec_len, lcp_M_len);
	if(descr_len!=0 || startpos_len!=0 || sa_len!=0 || isa_len!=0 || lcp_vec_len!=0 || lcp_M_len!=0) {
		cerr<<"Bug: decodeSAPart: (saa->descr).size != 0 || (saa->startpos).size != 0 || (saa->SA).size != 0 || (saa->ISA).size != 0 || (saa->LCP).vec.size != 0 || (saa->LCP).M.size != 0"<<endl;  
		exit(1);
	}

	// decode N, K, logN, NKm1
	//printf("decode N, K, logN, NKm1\n...\n");
	saa->N = buffer[0];
	saa->K = buffer[1];
	saa->logN = buffer[2];
	saa->NKm1 = buffer[3];
	realstart = realLen;
	realLen += 4;

	// decode descr, startpos
	//printf("decode descr, startpos\n...\n");
	long *pos_descr;
	pos_descr = buffer+realLen;

	startpos_len = pos_descr[0];
	for(long i=1; i<=startpos_len; i+=2) {
		(saa->descr).push_back(pos_descr[i]);
		(saa->startpos).push_back(pos_descr[i+1]);
	}
	realstart = realLen;
	realLen += (startpos_len+1);

	// decode SA, ISA
	//printf("decode SA, ISA\n...\n");
	long *sa_isa;
	sa_isa = buffer+realLen;

	sa_len = sa_isa[0];
	for(long i=1; i<=sa_len; i+=2) {
		(saa->SA).push_back(sa_isa[i]);
		(saa->ISA).push_back(sa_isa[i+1]);
	}
	realstart = realLen;
	realLen += (sa_len+1);

	// decode LCP vec 
	//printf("decode LCP.vec\n...\n");
	long *lcp_vec;
	lcp_vec = buffer+realLen;

	lcp_vec_len = lcp_vec[0];
	for(long i=1; i<=lcp_vec_len; i++) {
		(saa->LCP).vec.push_back((unsigned char)lcp_vec[i]);
	}
	realstart = realLen;
	realLen += (lcp_vec_len+1);

	// decode LCP M 
	//printf("decode LCP.M\n...\n");
	long *lcp_M;
	lcp_M = buffer+realLen;

	lcp_M_len = lcp_M[0];
	for(long i=1; i<=lcp_M_len; i+=2) {

		size_t idx = (size_t)lcp_M[i];
		int val = (int)lcp_M[i+1];
		vec_uchar::item_t itt(0,0); 
		itt.idx = idx;
		itt.val = val;
		(saa->LCP).M.push_back(itt);
		//(saa->LCP).M.push_back(vec_uchar::item_t(idx,val));
	}

	realstart = realLen;
	realLen += (lcp_M_len+1);

	/* the size_t is defined as unsigned int or unsigned long depending on system, so steps below are useless */
	//long offset = (long)(sizeof(size_t)/sizeof(long));
	//for(long i=1; i<=lcp_M_len; i+=offset+1) {

	//	size_t *idx = (size_t*)&lcp_M[i];
	//	int val = (int)lcp_M[i+offset];
	//	vec_uchar::item_t itt(0,0); 
	//	itt.idx = (*idx);
	//	itt.val = val;
	//	(saa->LCP).M.push_back(itt);
	//	//(saa->LCP).M.push_back(vec_uchar::item_t(idx,val));
	//}
	//realstart = realLen;
	//realLen += (lcp_M_len+1);

	//printf("size_t length = %d, long length = %d\n", (int)sizeof(size_t), (int)sizeof(int));
}

void decodeSA(paraSA *&saa, vector<Genome> &ref_genome, long *&buffer, char *&charbuff)
{

	if(charbuff==NULL || buffer == NULL || saa ==NULL) {
		cerr<<"Bug: decodeSA: pointer charbuff = NULL or buffer = NULL or pointer saa = NULL."<<endl;
		exit(1);
	}

	decodeSAPart(saa, buffer);
	decodeSAString(saa, ref_genome, charbuff);
}
