#include "encodeTG.hpp"

using namespace std;


//vector<GenomeClustInfo> totalgenomes; // Part info of total genomes.

void longTGRangeCheck(long curPos, long Max)
{
	if(curPos == Max)
	{
		cerr<<"longTGArray reach the Max Num of buffer."<<endl;
	}

	if(curPos > Max)
	{
		cerr<<"longTGArray overflow the buffer."<<endl;
		exit(0);
	}
}

/*
 * encode totalgenomes to an array of long typ for merge totalgenomes from master and worker.
 * encoding order:
 *	long real_num:	real length of the array after encode
 *		bool rep
 *		int  cUnits_size:	clusterunits number
 *		long id:		id of clusterunit
 *		double identity:	identity of clusterunit
 *		char strand:		strand of clusterunit
 */
long encodeTGtoMerge(long *&longTG, long longTG_Num, vector<GenomeClustInfo> &totalgenomes)
{
	if(longTG == NULL)
	{
		cerr<<"encodeTGtoMerge Error: null pointer."<<endl;
		exit(0);
	}

	long realLenOfArray = 0;
	long MaxLongNum = longTG_Num;
	long curPos = 1;// skip longTG[0] that is used to store real lenght of the array

	long MaxTGNum = totalgenomes.size();
	if(MaxTGNum <= 0)
	{
		cerr<<"encodeTGtoMerge Error: the number of elements of totalgenomes is 0"<<endl;
		exit(0);
	}

	for(long i=0; i<MaxTGNum; i++)
	{
		bool gi_rep = totalgenomes[i].rep;
		if(gi_rep)
		{// rep is true

			// encode rep
			longTG[curPos] = 1;
			curPos++;
			longTGRangeCheck(curPos, MaxLongNum);

			// get clusterunits number
			long cUnits_size = totalgenomes[i].clusterunits.size();
			if(cUnits_size == 0)
			{
				// encode clusterunits number
				longTG[curPos] = 0;
				curPos++;	
				longTGRangeCheck(curPos, MaxLongNum);
			}
			else if(cUnits_size > 0)
			{
				// encode clusterunits number
				longTG[curPos] = cUnits_size;
				curPos++;
				longTGRangeCheck(curPos, MaxLongNum);

				// encode each clusterunit
				vector<hit> &cUnits = totalgenomes[i].clusterunits;
				for(long j=0; j<cUnits_size; j++)
				{
					// get id
					long id = cUnits[j].id;

					// encode id
					longTG[curPos] = id;//id's type is long. different systems are presented as 32 or 64 bits.
					curPos++;
					longTGRangeCheck(curPos, MaxLongNum);

					// get identity 
					double identity = cUnits[j].identity;

					// encode identity
					if(sizeof(long) == sizeof(double))
					{
						long *iden;
						iden = (long*)&identity;
						longTG[curPos] = (*iden);
						curPos++;
						longTGRangeCheck(curPos, MaxLongNum);
					}
					else if(2*sizeof(long) == sizeof(double))
					{
						long *iden;
						iden = (long*)&identity;
						longTG[curPos] = (*iden);
						curPos++;

						iden++;
						longTG[curPos] = (*iden);
						curPos++;

						longTGRangeCheck(curPos, MaxLongNum);
					}

					// encode strand
					if(cUnits[j].strand == '+') {
						longTG[curPos] = 1;
					}
					else {
						longTG[curPos] = 0;
					}
					curPos++;
					longTGRangeCheck(curPos, MaxLongNum);

				}
			}
			else
			{
				cerr<<"Invalid number of clusterunits for Seq "<<i<<endl;
				exit(0);
			}
		}
		else
		{//rep is false

			// encode rep
			longTG[curPos] = 0;
			curPos++;
			longTGRangeCheck(curPos, MaxLongNum);
		}
	}

	realLenOfArray = curPos;
	longTG[0] = realLenOfArray;

	return realLenOfArray;
} 

/*
 * compare clusterunits by the id
 */
bool cmp_clusterunits(const hit &clusterunit0, const hit &clusterunit1)
{
	return clusterunit0.id < clusterunit1.id;
}

/*
 * merge totalgenomes
 * decoding a GenomeClustInfo, then check and merge to totalgenomes
 *	totalgenomes: totalgenomes in master
 *	long tgIndex: totalgenomes index which need to merge
 *	gi:	decode from totalgenomes in worker
 */
void mergeTG(vector<GenomeClustInfo> &totalgenomes, long tgIndex, GenomeClustInfo &gi)
{
	// checking has done out of the func

	//if(totalgenomes[tgIndex].rep == 0 && gi.rep == 0)
		// do nothing

	//if(totalgenomes[tgIndex].rep == 0 && gi.rep == 1)
		// do nothing

	if(totalgenomes[tgIndex].rep == 1 && gi.rep == 0) {
		// instead totalgenomes[tgIndex] with gi
		totalgenomes[tgIndex].rep = gi.rep;
		totalgenomes[tgIndex].clusterunits.clear();// it is necessary
	}

	if(totalgenomes[tgIndex].rep == 1 && gi.rep == 1) {

		int Size_cUnitsOfgi = (int)gi.clusterunits.size();

		if(Size_cUnitsOfgi > 0) {

			// merge clusterunits of both totalgenomes[tgIndex] and gi
			for(int i=0; i<Size_cUnitsOfgi; i++) {
				totalgenomes[tgIndex].clusterunits.push_back(gi.clusterunits[i]);
			}

			// sort the clusterunits in totalgenomes by id
			std::sort(totalgenomes[tgIndex].clusterunits.begin(), totalgenomes[tgIndex].clusterunits.end(), cmp_clusterunits);
		}
	}
}

/*
 * decode longTG and merge totalgenomes
 * decoding order:
 *	long real_num:	real length of the array after encode
 *		bool rep
 *		int  cUnits_size:	clusterunits number
 *		long id:		id of clusterunit
 *		double identity:	identity of clusterunit
 *		char strand:		strand of clusterunit
 */
void decodeLongTG_Merge(vector<GenomeClustInfo> &totalgenomes, long *&longTG, long longTG_Num)
{
	if(longTG == NULL){
		cerr<<"decodeLongTG_Merge Error: null pointer."<<endl;
		exit(0);
	}

	long MaxLongNum = longTG_Num;
	long RealNum = longTG[0];//realNum;
	longTGRangeCheck(RealNum, MaxLongNum);

	long curPos = 1;// skip longTG[0] that stored real lenght of the array

	GenomeClustInfo gi;// used for decode and merge

	long MaxTGNum = totalgenomes.size();
	if(MaxTGNum <= 0)
	{
		cerr<<"decodeLongTG_Merge Error: the number of elements of totalgenomes is 0, the size have to be > 0 to merge"<<endl;
		exit(0);
	}

	long tgIndex = 0;

	while(curPos < RealNum)
	{
		gi.clusterunits.clear();// make sure there is no element in clusterunits for each loop

		long rep = longTG[curPos];
		if(rep == 1)
		{
			// decode rep
			gi.rep = 1;
			curPos++;

			// decode clusterunits
			long cUnits_size = longTG[curPos];
			curPos++;

			if(cUnits_size > 0)
			{
				for(long i=0; i<cUnits_size; i++)
				{
					hit chit;

					// decode id
					long *cid;
					cid = (long*)&longTG[curPos];
					chit.id = (*cid);
					curPos++; 

					// decode identity
					double *identity;
					if(sizeof(long) == sizeof(double))
					{
						identity = (double*)&longTG[curPos];
						chit.identity = (*identity);
						curPos ++;
					}
					else if(2*sizeof(long) == sizeof(double))
					{
						identity = (double*)&longTG[curPos];
						chit.identity = (*identity);
						curPos +=2;
					}

					// decode strand
					long strand = longTG[curPos];
					if(strand == 1)
					{
						chit.strand = '+';
					}
					else
					{
						chit.strand = '-';
					}
					curPos++; 

					gi.clusterunits.push_back(chit);
				}
			}

		}
		else
		{
			// decode rep
			gi.rep = 0;
			curPos++;

		}

		//merge gi to totalgenomes
		mergeTG(totalgenomes, tgIndex, gi);

		tgIndex++;
		if(tgIndex > MaxTGNum)
			cerr<<"decodeLongTG_Merge Error: elements of totalgenomes overflows."<<endl;

	}
	
	return ;
}

