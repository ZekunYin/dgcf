#include "similarity.hpp"

using namespace std;
int K = 8; // Note: Using sparse suffix array for larger chunk size.
int Nchunk = 10; // load one time for remaining genomes clustering.
int min_len = 20; // Default minimum exact match length.
int MEMiden = 90; // Default identity cutoff.
int total_threads = 1; // Threads number.

// MEM extension parameters
int ext = 1; // noextension, gap or ungap extension
int mas = 1; // Match score
int umas = -1; // Mismatch cost
int gapo = -1; // Gap open penalty
int gape = -1; // Gap extension penalty
int drops = 1; // Maximum score drop

bool rev_comp = false;
bool nucleotides_only = false;
bool rebuild = false; // Rebuild suffix array into one part.


void prepare_for_bcast(vector<Genome> &genomes,
					   vector<GenomeClustInfo> &totalgenomes,
					   long *&mpi_buffer, char *&str_buffer,
					   long &max_buffer_size, long &max_str_size,
					   long &buffer_len, long &str_len,
					   algorithm alg,
			  		   int *&mask_tg)
{
	switch(alg)
	{
		case cdhit: prepare_for_cdhit(); break;
		case gclust: prepare_for_gclust(genomes,totalgenomes,mpi_buffer,str_buffer,max_buffer_size,max_str_size,buffer_len,str_len,mask_tg); break;
		default: cerr << "alg is not defined" << endl;
	}
	

}

void prepare_and_search(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id, algorithm alg){
	switch(alg){
		case cdhit: prepare_and_search_cdhit(refgenomes,qrygenomes,
							totalgenomes, mpi_buffer, str_buffer,
							chunk_id);
					break;
		case gclust: prepare_and_search_gclust(refgenomes,qrygenomes,
							totalgenomes, mpi_buffer, str_buffer,
							chunk_id);
					break;
		default: cerr << "alg is not defined" << endl;
	}

}
void prepare_and_search_cdhit(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id){

	cerr << "not done yet" << endl;
}
void prepare_and_search_gclust(vector<Genome> &refgenomes,
							vector<Genome> &qrygenomes,
							vector<GenomeClustInfo> &totalgenomes,
							long *&mpi_buffer,
							char *&str_buffer,
							int chunk_id)
{
	refgenomes.clear();
	string ref ="";
	//int K = 8;
	int chunk_start = 0;
	vector<long> descr;	
	vector<long> startpos;
	bool clusterhit = false;
	paraSA *saa = new paraSA(ref,descr,startpos,true,K);
	decodeSA(saa,refgenomes,mpi_buffer,str_buffer);

	//calc chunk_start
//	while(qrygenomes[chunk_start].chunk_id <= chunk_id)
//		chunk_start ++;
	for(int i = 0; i < qrygenomes.size();i++){
		if(qrygenomes[chunk_start].chunk_id <= chunk_id)
			chunk_start ++;
		else 
			break;
	}
	cerr << "chunk_id " << chunk_id << "chunk_start" <<	chunk_start << endl;
	mt_gclust_search(saa,refgenomes,qrygenomes,totalgenomes,chunk_start);

	//tg.cluster -> tg.clusterunits
	getClusteringInfoOnepart(totalgenomes,qrygenomes,0,qrygenomes.size(),clusterhit);
	delete saa;
}

void prepare_for_cdhit(){

}

void prepare_for_gclust(vector<Genome> &refgenomes, 
						vector<GenomeClustInfo> &totalgenomes,
				       long *&mpi_buffer, char* &str_buffer,
					   long &max_buffer_size, long &max_str_size,
					   long &buffer_len, long &str_len,
					   int *&mask_tg)
{
	cerr << "start to prepare data for gclust " << endl;
	//int K = 10; //sparse
	string ref = "";
	vector<long> descr;
	vector<long> startpos;	
	bool clusterhit = false;
	//cerr << "try wo access mask_tg mpi_buffer and str_buffer" << endl;
	make_block_ref(refgenomes, totalgenomes, ref, mask_tg, descr,startpos);	
	paraSA *saa = new paraSA(ref,descr,startpos,true,K);
	saa->buildSA();
	//inner cluster
	
	//end inner cluster
	cerr << "start inner search" << endl;
	mt_inner_gclust_search(saa, refgenomes,totalgenomes);

	getClusteringInfoOnepart(totalgenomes,refgenomes,0, refgenomes.size(), clusterhit);
						
	//rebuild saa
	if(rebuild && clusterhit){
		delete saa;	
		ref="";
		descr.clear();
		startpos.clear();

		make_block_ref(refgenomes, totalgenomes, ref, mask_tg, descr,startpos);	
		paraSA *saa = new paraSA(ref,descr,startpos,true,K);
		saa->buildSA();
	}	
	encodeSA(mpi_buffer, str_buffer,buffer_len,str_len,max_buffer_size,saa);//prepared for MPI_Bcast


/***************************test encode and decode SA*********************************/
//	string refb ="";
//	vector<long>descrb;	
//	vector<long> startposb;
//	vector<Genome> genomesb;
//	paraSA *sab = new paraSA(refb,descrb,startposb,true,K);
//	decodeSA(sab,genomesb,mpi_buffer,str_buffer);
//	
//	//check genomes
//	if(genomes.size() != genomesb.size())
//		cerr << "genomes size not equal" << endl;
//	else
//		cerr << "genomes size check passed" << endl;
//	for(int i = 0; i < genomes.size();i++)
//		if(genomes[i].cont != genomesb[i].cont)
//			cerr << "cont " << i << " not equal"  << endl;
//
//	cerr << "genomes check passed" << endl;	
//	//check SA
//	
//	if(saa->descr.size() == sab->descr.size())
//		cerr << "SA decsr size passed" << endl;
//	else 
//		cerr << "SA descr size not equal" << endl;		
//	
//	if(saa->startpos.size() == sab->startpos.size())
//		cerr << "SA startpos size passed" << endl;
//	else 
//		cerr << "SA startpos size not equal" << endl;		
//
//	if(saa->SA.size() == sab->SA.size())
//		cerr << "SA size passed" << endl;
//	else 
//		cerr << "SA size not equal" << endl;		
//
//	if(saa->ISA.size() == sab->ISA.size())
//		cerr << "ISA size passed" << endl;
//	else 
//		cerr << "ISA size not equal" << endl;		
//
//	
//	delete sab;	
/***************************test encode and decode SA*********************************/
	cerr << "real buffer size is " << buffer_len <<endl;	
	cerr << "real str size is " << str_len <<endl;	

	delete saa;
		
}

//copy data back from mpi buffer
void mt_gclust_search(paraSA *&saa, vector<Genome> &refgenomes, 
					 vector<Genome> &qrygenomes,
					 vector<GenomeClustInfo> &totalgenomes,
					 int &chunk_start){
	if(chunk_start > qrygenomes.size())
		cerr << "search start position out of range " << endl;	
	cerr << "GCLUST search start " << chunk_start << " to " << qrygenomes.size() << endl;
	
	int count = 0;
	int comp_count = 0;
	int i;
#pragma omp parallel for num_threads(12) default(shared) private(i) shared(saa) schedule(dynamic)
	for(i = chunk_start; i < qrygenomes.size(); i++){
	//	comp_count++;
	//	if(comp_count % 100 == 0)
	//		cerr << "============" << comp_count << " DONE" << endl;
		if(!totalgenomes[qrygenomes[i].id].rep){
	//		count++;
			continue;
		}
		string *P = new string;
		bool ifhit = false;
		long sizeadd = 0;

		// Match information container.
		vector<match_t> matches;
		// Mem index container.
		vector<mumi_unit> mumis;
		Genome tg = qrygenomes[i];
	
		//default parametes been defined	
		double cutoff = (double) MEMiden / 100;
	
		//filter_n(*P);
		*P = tg.cont;
	    //cerr << "P.size " << P->size() << endl;	
		if(MEMiden == 100)
			saa->MEMperfect(*P, matches, tg.size, tg.id);
		else
			saa->MEM(*P,matches,min_len,tg.id);
							
		sizeadd += saa->load_match_info(tg.id, matches, mumis, true, tg.size);
		matches.clear();
		cerr << tg.id  << "---" <<(double) sizeadd / tg.size << endl;
		if((double) sizeadd / tg.size >= cutoff)
		{
			ifhit=ComputeMemIdentity(totalgenomes, 
						refgenomes, 
						qrygenomes,
						mumis, 
						0,
						tg.id,
						i, 
						MEMiden, 
						false,
						refgenomes.size(),
						'+', 
						ext, 
						mas,
						umas, 
						gapo, 
						gape,
						drops);
		}
		
		mumis.clear();
		sizeadd=0;
	
//add reverse a bit later
//		if(!ifhit)
//		{
//		
//		}	
	
		delete P;
	}

	cout << count << " jumped findMEM" << endl;

}
void mt_inner_gclust_search(paraSA *&saa, 
					 vector<Genome> &refgenomes, 
					 vector<GenomeClustInfo> &totalgenomes){

	//search inner refgenomes	
	int i;
	cerr << "inner size " << refgenomes.size() << endl;
	//jump the first search
#pragma omp parallel for default(shared) private(i) shared(saa) schedule(dynamic)
	for( i = 1; i < refgenomes.size();i++){
		if(!totalgenomes[refgenomes[i].id].rep){
			continue;	
		}
		
		string *P = new string;
		bool ifhit = false;
		long sizeadd = 0;
		vector<match_t> matches;
		matches.reserve(1000000);
		vector<mumi_unit> mumis;
		mumis.reserve(1000000);
		Genome tg = refgenomes[i];

		double cutoff = (double) MEMiden / 100;
			
		*P = tg.cont;
		//cerr << "P.size " << P->size() << endl;
		cerr << "id " << tg.id << endl;
		//cerr << "min_len " << min_len << endl;
		//cerr << "matches.size " << matches.size() << endl;
		if(MEMiden == 100)
			saa->MEMperfect(*P, matches, tg.size, tg.id);
		else
			saa->MEM(*P,matches,min_len,tg.id);
							
		sizeadd += saa->load_match_info(tg.id, matches, mumis, true, tg.size);
		matches.clear();
		cerr << tg.id  << "inner" <<(double) sizeadd / tg.size << endl;

		if((double) sizeadd / tg.size >= cutoff)
		{
			ifhit=ComputeMemIdentity(totalgenomes, 
						refgenomes, 
						refgenomes,
						mumis, 
						0,
						tg.id,
						i, 
						MEMiden, 
						true,
						refgenomes.size(),
						'+', 
						ext, 
						mas,
						umas, 
						gapo, 
						gape,
						drops);
		}
		
		mumis.clear();
		sizeadd=0;
	
//add reverse a bit later
//		if(!ifhit)
//		{
//		
//		}	
	
		delete P;

	}
}
