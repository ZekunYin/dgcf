#ifndef __ENCODESA__
#define __ENCODESA__
#include "paraSA.hpp"
#include "fasta.hpp"

void printfSAString(paraSA *&saa);
void deleteSAString(paraSA *&saa);
void encodeSAString(char *&charbuff, long &rlen, paraSA *&saa);

void decodeSAString(paraSA *&saa, vector<Genome> &ref_genome, char *&charbuff);
void printfSA(paraSA *&saa, long descr_len, long startpos_len, long sa_len, long isa_len, long lcp_vec_len, long lcp_M_len);
void printfSA(paraSA *&saa);
void deleteSA(paraSA *&saa);
void encodeSAPart(long *&buffer, long _BufferSize, paraSA *&saa, long &rlen);

void encodeSA(long *&buffer, char *&charbuff, long &rlen, long &crlen, long _BufferSize, paraSA *&saa);
void decodeSAPart(paraSA *&saa, long *&buffer);
void decodeSA(paraSA *&saa, vector<Genome> &ref_genome, long *&buffer, char *&charbuff);
#endif
