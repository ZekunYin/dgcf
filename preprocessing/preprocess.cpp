
/****************************

  Gclust Preprocess: sort genomes and make block

 ****************************/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>
#include <cctype>
#include <algorithm>
#include <sstream>
#include <string>

using namespace std;

void usage(string prog);

// Genomes info container.
struct GenomeInfo
{
	long id; //global index
	long length; //genome length
	string descript; //genome name
	long fseek;
	long chk_id;
};

// Genomes chunk info.
struct GenomeCHInfo
{
	long id; //global index
	long length; //genome length
	long fseek;
};

//global variables
int nodenum = 3;//Default node number
int chunk = 100;
int thread_num = 1;
int BASE = 1000000;// 100 MB
int chunk_size = chunk*BASE;//Default chunk size
int MAXSeqNum = 0;
long MAXChunkSize = 0;

vector<GenomeInfo> totalgenomes; // Part info of total genomes.
vector< vector<GenomeCHInfo> > chunkinfo;
vector< vector<int> > block;

vector<long> chunkSize;
vector<int> seqNumber;

// long to string without cxx11 support.
string long_to_str(long a)
{

	ostringstream st;
	st << a;

	return st.str();
}

// trim a string.
void trim(string &line, long &start, long &end) 
{
	// Trim leading spaces. 
	for(long i = start; i < (int)line.length(); i++) 
	{ 
		if(line[i] != ' ') 
		{ 
			start = i; 
			break;
		} 
	}
	// Trim trailing spaces.
	for(long i = line.length() - 1; i >= 0; i--) { 
		if(line[i] != ' ') { 
			end = i;
			break;
		} 
		if(i == 0) break;
	}
}

// Partition, used in QuickSort. To confirm the middle position.
long Partition(vector<GenomeInfo> &totalgenomes, long low, long high)
{
	long id = totalgenomes[high].id;
	long len = totalgenomes[high].length;
	string name = totalgenomes[high].descript;
	long seek = totalgenomes[high].fseek;

	int i = low - 1;// the last position of element whose value < len.
	for(int j = low; j < high; j++)
	{
		long y = totalgenomes[j].length;
		if(y > len)
		{
			GenomeInfo gi;
			i++;

			gi.id = totalgenomes[i].id;
			gi.length = totalgenomes[i].length;
			gi.descript = totalgenomes[i].descript;
			gi.fseek = totalgenomes[i].fseek;

			totalgenomes[i].id = totalgenomes[j].id;
			totalgenomes[i].length = totalgenomes[j].length;
			totalgenomes[i].descript = totalgenomes[j].descript;
			totalgenomes[i].fseek = totalgenomes[j].fseek;

			totalgenomes[j].id = gi.id;
			totalgenomes[j].length = gi.length;
			totalgenomes[j].descript = gi.descript;
			totalgenomes[j].fseek = gi.fseek;

		}
	}

	totalgenomes[high].id = totalgenomes[i+1].id; 
	totalgenomes[high].length = totalgenomes[i+1].length; 
	totalgenomes[high].descript = totalgenomes[i+1].descript; 
	totalgenomes[high].fseek = totalgenomes[i+1].fseek; 

	totalgenomes[i+1].id = id; 
	totalgenomes[i+1].length = len; 
	totalgenomes[i+1].descript = name; 
	totalgenomes[i+1].fseek = seek; 

	return i+1;
}

// QuickSort, sort the totalgenomes from start to end.
void QuickSort(vector<GenomeInfo> &totalgenomes, long low, long high)
{
	if(low < high) {
		long q = Partition(totalgenomes, low, high);
		QuickSort(totalgenomes, low, q-1);
		QuickSort(totalgenomes, q+1, high);
	}	
}

// Sort part information of total genomes by genome's length.
void sortByGenomeLength(vector<GenomeInfo> &totalgenomes)
{
	GenomeInfo gi;

	long start = 0;
	long end = totalgenomes.size()-1;

	QuickSort(totalgenomes, start, end);

}
// Load part information of total genomes one time.
void loadPartInfoOfTotalGenomes(string filename, vector<GenomeInfo> &totalgenomes)
{
	// the variables bellow are not used.
	long maxlen, minlen, totallen;
	maxlen=totallen=0;
	minlen=0;

	int loadnum = 0; //

	long id = 0;
	long length = 0;
	long seq_fseek = -1;
	string name, line;

	GenomeInfo tg;

	// Everything starts at zero.
	ifstream data(filename.c_str());
	if(!data.is_open()) { 
		cerr << "unable to open " << filename << endl; 
		exit(1); 
	}

	cerr<<"\n\n>>>>>>Reading Genome File.\n\n";
	cerr<<"Loading Genome info ......"<<endl;

	//int debug = 1;
	while(!data.eof()) {
		//if(id>499) break;// used to debug and test

		long cur_fseek = data.tellg();

		getline(data, line); // Load one line at a time.
		if(line.length() == 0) continue;

		long start = 0, end = line.length() - 1;

		/*
		// just to test the basic unit, useless.
		if(debug == 0) {
		cerr << "the id is " << id <<endl; 
		char bp = line[0];
		cerr << "the basic unit is char - " << sizeof(bp) << " byte. eg:" << bp <<endl; 
		cerr << "the basic unit is line[0] - " << sizeof(line[0]) << " byte. eg:" << line[0] <<endl; 
		debug = 1;
		}
		*/

		if(line[0] == '>') { // Meta tag line and start of a new sequence.

			if(length > 0) { 
				//get max and min length of sequences
				if(id == 0)	
				{
					maxlen = length;
					minlen = length;
				}
				else{
					if(length > maxlen) maxlen = length;
					if(length < minlen) minlen = length;
				}
				totallen += length;

				// Save previous sequence and meta data.
				tg.descript=name;
				tg.id=id;
				tg.length=length;
				totalgenomes.push_back(tg);
				id++;


			}
			start = 1; name = ""; length = 0; // Reset parser state.
		}

		trim(line, start, end);

		if(line[0] == '>') { // Collect name.
			if(id%1000 == 0)
				cerr<<"	Loading Genome info ......"<<id<<endl;

			if(length == 0) {
				//	cerr<<"file_fseek: "<<cur_fseek<<endl;
				seq_fseek = cur_fseek;
				tg.fseek = seq_fseek;
			}

			for(long i = start; i <= end; i++) { 
				if(line[i] == ' ') break; 
				name += line[i];
			}
		}else { // Collect sequence data.
			length += end - start + 1;

		}
	}

	if(length > 0) { // Last one.
		//get max and min length of sequences
		if(id == 0)	
		{
			maxlen = length;
			minlen = length;
		}
		else{
			if(length > maxlen) maxlen = length;
			if(length < minlen) minlen = length;
		}
		totallen += length;

		tg.descript=name;
		tg.id=id;
		tg.length=length;
		totalgenomes.push_back(tg);
		id++;
	}

	data.close();

	loadnum = id;

	cerr<<"	====================="<<endl;
	cerr<<"	Genomes: "<<loadnum<<endl;
	cerr<<"	Maximum Length: "<<maxlen<<endl;
	cerr<<"	Minimum Length: "<<minlen<<endl;
	cerr<<"	Total Length: "<<totallen<<endl;
	cerr<<"	Average Length: "<<totallen/loadnum<<endl;
	cerr<<"	====================="<<endl;
	cerr<<endl;

	if( chunk_size < maxlen){
		cerr << "ERR!!! Chunk_size is: "<< chunk_size <<endl;
		cerr << "ERR!!! Chunk_size must be larger than maximum length = " << maxlen << endl;
		exit(1);
	}

}

// Output part information of total genomes.
void outputPartInfoOfTotalGenomes(string filename, vector<GenomeInfo> &totalgenomes)
{
	long id = 0;
	long length = totalgenomes.size();
	string name, line;

	GenomeInfo tg;

	// Open the file.
	cerr << "	Open file,  " << filename << endl; 

	ofstream data(filename.c_str());
	if(!data.is_open()) { 
		cerr << "unable to open " << filename << endl; 
		exit(1); 
	}

	// Write one line at a time.
	cerr << "	Write file, name " << filename << endl; 
	cerr<<"	Output config info MAXChunkSize, MAXSeqNum ......"<<endl;

	string config_line = long_to_str(MAXChunkSize) +" "+ long_to_str((long)MAXSeqNum) + "\n";// MAXChunkSize, MAXSeqNum is global var.
	data<<config_line;

	while(1) {
		if(id%1000 == 0)
			cerr<<"	Output part info ......"<<id<<endl;

		if(id >= length){
			break;
		}	
		totalgenomes[id].id = id;//zk fix bugs	
		string sid = long_to_str(totalgenomes[id].id);
		string slength = long_to_str(totalgenomes[id].length);
		string sdescript = totalgenomes[id].descript;
		string sfseek = long_to_str(totalgenomes[id].fseek);
		string schk_id = long_to_str(totalgenomes[id].chk_id);
		//cerr<<"output fseek: "<<totalgenomes[id].fseek<<endl;

		//line = "" + sid + " " + slength + " " + totalgenomes[id].descript + " " + sfseek + "\n";
		line = "" + schk_id + " " + sid + " " + slength + " " + totalgenomes[id].descript + "\n";
		//cerr << line << endl; 

		if(line.length() == 0) {
			continue;
		}
		else {
			data << line;
		}

		id++;

	}

	data.close();

	cerr<<"	=====================\n";
	cerr<<"	TotalLines: "<<length<<endl;
	cerr<<"	WriteLines: "<<id<<endl;
	cerr<<"	=====================\n";
	cerr<<"	Write file end.\n\n"<<endl; 

}


void renumberTotalgenomesId(vector<GenomeInfo> &totalgenomes)
{
	long id = 0;
	long length = totalgenomes.size();

	while(1) {

		if(id >= length){
			break;
		}	
		totalgenomes[id].id = id;//zk fix bugs	

		id++;

	}
}

// Output the information of chunks.
void outputChunkInfo(string filename, vector< vector<GenomeCHInfo> > &chunkinfo, int nodenum)
{
	unsigned int stepOfoutput = 50;
	cerr<<endl; 
	cerr<<"========================================================\n";
	cerr<<"Output chunk info, "<<filename<<endl; 
	cerr<<"Each seq of chunk is output in "<< stepOfoutput << " steps ... "<<endl; 
	cerr<<endl; 
	//cerr<<"gid  len  ch_id  fseek\n";
	printf("%-15s  %-15s  %-15s  %-15s\n", "ch_id", "gid", "len", "fseek");
	for(unsigned int i=0; i<chunkinfo.size(); i++)
	{
		for(unsigned int j=0; j<chunkinfo[i].size(); j++)
		{
			// output by step 50 in one chunk.
			if( (j % stepOfoutput) ==0 )
				//cerr<<chunkinfo[i][j].id<<" "<<chunkinfo[i][j].length<<" "<<i<<" "<<chunkinfo[i][j].fseek<<endl;
				printf("%-15ld  %-15ld  %-15ld  %-15ld\n", (long)i, chunkinfo[i][j].id, chunkinfo[i][j].length, chunkinfo[i][j].fseek);
		}
		printf("\n");
	}
	cerr<<endl; 
	cerr<<"Output chunk info end."<<endl;
	cerr<<"========================================================\n";
	cerr<<endl; 
}

// insert one seq to new subfile.
void insertSingleSeq(ofstream &subdata, ifstream &data, string &bline, long data_fseek)
{
	//cerr<<"insert single seq"<<endl;
	data.seekg(data_fseek, data.beg);
	string line = "";
	string name = "";
	getline(data, line);

	if(line[0] == '>')
	{
		long start = 0, end = line.length() - 1;
		for(long i = start; i <= end; i++) { 
			if(line[i] == ' ') break; 
			name += line[i]; 
		}

		line = name+" "+bline;
		subdata<<line;

		while(1)
		{
			getline(data, line);
			//cerr<<"line in while"<<line<<endl;
			line += "\n";
			if(data.eof() || line[0] == '>') {
				if(data.eof()) {data.clear();}
				break;
			}
			subdata<<line;
		}
	}
	else
	{
		cerr<<"insert the single seq without the line whose first character is \' > \' ."<<endl;
	}
} 
// Output the information of block.
void outputBlockInfo(string filename, vector< vector<int> > &block, vector< vector<GenomeCHInfo> > &chunkinfo)
{
	cerr<<endl; 
	cerr<<"Output block information ......"<<endl;
	ifstream data(filename.c_str());
	if(!data.is_open()) { 
		cerr << "unable to open " << filename << endl; 
		exit(1); 
	}

	for(unsigned int k=0; k<block.size(); k++)
	{
		unsigned int stepOfoutput = 1000;
		string subfilename = filename + "." + long_to_str(k);
		cerr << "	Write sub file, name " << subfilename << endl; 
		cerr << "	Property : global id, seq length, seq fseek(in source file), chunk id, block id" << endl; 
		cerr << "	Output insert seq by step of "<< stepOfoutput <<endl; 

		// Open the files.
		ofstream subdata(subfilename.c_str());
		if(!subdata.is_open()) { 
			cerr << "unable to open " << subfilename << endl; 
			exit(1); 
		}

		// write the block info and split the original data to new sub files.
		for(unsigned int m=0; m<block[k].size(); m++)
		{
			int chunk_id = block[k][m];
			for(unsigned int n=0; n<chunkinfo[chunk_id].size(); n++)
			{

				string gid = long_to_str(chunkinfo[chunk_id][n].id);
				string length = long_to_str(chunkinfo[chunk_id][n].length);
				string fseek = long_to_str(chunkinfo[chunk_id][n].fseek);
				string schunk_id = long_to_str(chunk_id);
				string block_id = long_to_str(k);

				string line = "" + gid + " " + length + " " + fseek + " " + schunk_id + " " + block_id +"\n";
				if( ((m*n)%(stepOfoutput)) == 0 )
					cerr<<"		Insert seq "<<gid<<" to the sub-file: "<<line;

				string bline = "" + gid + " " + schunk_id + " " + block_id + "\n";

				//in this place, need to insert a function which can insert a seq to new sub file.
				insertSingleSeq(subdata, data, bline, chunkinfo[chunk_id][n].fseek);
			}
		}

		// Close the files.	
		subdata.close();
		cerr<<"\n\n"<<endl;
	}

	data.close();

}
// make chunk by chunk size.
void makeChunk(vector< vector<GenomeCHInfo> > &chunkinfo, long &MAXChunkSize, int &MAXSeqNum, vector<GenomeInfo> &totalgenomes, long chunk_size, int _thread_num)
{

	int seqNum = totalgenomes.size();
	int thread_num = _thread_num;

	long sizeadd = 0;
	long CHUNK_SIZE = chunk_size;
	long chunk_id = 0;

	GenomeCHInfo chi;

	// add first chunk.
	vector<GenomeCHInfo> chunkinfoUnits;
	chunkinfo.push_back(chunkinfoUnits);

	cerr<<"\n"<<endl;
	cerr<<"New the "<<chunk_id<<"th chunk. Chunk number is "<<chunkinfo.size()<<endl;

	int i = 0;//index of first seq in totalgenomes hasn't been push back into any chunk
	int seqNumOfchunk = 0;

	while(i<seqNum)
	{
		if(chunk_id>=(long)chunkinfo.size()) {
			cerr<<"The index of current chunk is out of the range of the vector of chunks."<<endl;
			exit(1);
		}

		sizeadd += totalgenomes[i].length;
		//cout<<"tg "<<i<<" = "<<totalgenomes[i].length<<endl;
		//cout<<"1.sizeadd = "<< sizeadd <<endl;
		if(sizeadd<=CHUNK_SIZE) {
			//cerr<<"Chunk "<<chunk_id<<" add a seq "<<i<<" -> "<<totalgenomes[i].id<<"."<<endl;
			totalgenomes[i].chk_id = chunk_id;
			chi.id = totalgenomes[i].id;
			chi.length = totalgenomes[i].length;
			chi.fseek = totalgenomes[i].fseek;

			chunkinfo[chunk_id].push_back(chi);

			seqNumOfchunk++;
			i++;

			if(i>=seqNum) {
				long chunksize = sizeadd;
				chunkSize.push_back(chunksize);
				if(chunksize>MAXChunkSize) {
					MAXChunkSize = chunksize;
				}

				seqNumber.push_back(seqNumOfchunk);
				if(seqNumOfchunk>MAXSeqNum) {
					MAXSeqNum = seqNumOfchunk;
				}
				break;
			}
		}
		else {
			int rmd = seqNumOfchunk%thread_num;
			if(rmd!=0) {

				int iter = thread_num-rmd;
				for(int j=0; j<iter; j++) {
					totalgenomes[i].chk_id = chunk_id;
					chi.id = totalgenomes[i].id;
					chi.length = totalgenomes[i].length;
					chi.fseek = totalgenomes[i].fseek;

					chunkinfo[chunk_id].push_back(chi);

					seqNumOfchunk++;
					i++;

					//cout<<"tg "<<i<<" = "<<totalgenomes[i].length<<endl;
					//cout<<"2.sizeadd = "<< sizeadd <<endl;
					if(i>=seqNum) {
						long chunksize = sizeadd;
						chunkSize.push_back(chunksize);
						if(chunksize>MAXChunkSize) {
							MAXChunkSize = chunksize;
						}

						seqNumber.push_back(seqNumOfchunk);
						if(seqNumOfchunk>MAXSeqNum) {
							MAXSeqNum = seqNumOfchunk;
						}
						break;
					}
					sizeadd += totalgenomes[i].length;
				}
			}
			else {
				if(seqNumOfchunk==0) {

					for(int j=0; j<thread_num; j++) {
						totalgenomes[i].chk_id = chunk_id;
						chi.id = totalgenomes[i].id;
						chi.length = totalgenomes[i].length;
						chi.fseek = totalgenomes[i].fseek;

						chunkinfo[chunk_id].push_back(chi);

						seqNumOfchunk++;
						i++;

						//cout<<"tg "<<i<<" = "<<totalgenomes[i].length<<endl;
						//cout<<"3.sizeadd = "<< sizeadd <<endl;
						if(i>=seqNum) {
							long chunksize = sizeadd;
							if(chunksize>MAXChunkSize) {
								MAXChunkSize = chunksize;
							}

							seqNumber.push_back(seqNumOfchunk);
							if(seqNumOfchunk>MAXSeqNum) {
								MAXSeqNum = seqNumOfchunk;
							}
							break;
						}
						sizeadd += totalgenomes[i].length;
					}
				}
			}

			//just for jump the loop of while
			if(i>=seqNum) {
				break;
			}

			long chunksize = sizeadd - totalgenomes[i].length;
			chunkSize.push_back(chunksize);
			if(chunksize>MAXChunkSize) {
				MAXChunkSize = chunksize;
			}

			seqNumber.push_back(seqNumOfchunk);
			if(seqNumOfchunk>MAXSeqNum) {
				MAXSeqNum = seqNumOfchunk;
			}

			sizeadd = 0;// empty the size
			seqNumOfchunk = 0;// empty the number of current chunk
			chunk_id++;// new chunk id

			vector<GenomeCHInfo> chunkinfoUnits;
			chunkinfo.push_back(chunkinfoUnits);

			cerr<<"New the "<<chunk_id<<"th chunk. Chunk number is "<<chunkinfo.size()<<endl;
		}

	}
	cerr<<"\n\n"<<endl;
	/*
	// just for output the information of chunk size and chunk seq number
	if(chunkSize.size() == seqNumber.size()) {
		cerr<<"Output chunk size and chunk seq number ... , chunkSize == seqNumber"<<endl;
		for(unsigned int i = 0; i<chunkSize.size(); i++) {
			cerr<<chunkSize[i]<<"  "<<seqNumber[i]<<endl;
		}
	}
	*/
}

// make block by chunkinfo & nodenum.
void makeBlock(vector< vector<int> > &block, vector< vector<GenomeCHInfo> > &chunkinfo, int nodenum)
{
	for(int i=0; i<nodenum; i++)
	{
		vector<int> blockU;
		block.push_back(blockU);
	}

	int BlockNum = nodenum;
	int ChunkNum = chunkinfo.size();

	int block_id; 
	for(int k=0; k<ChunkNum; k++)
	{
		block_id = (k%BlockNum);
		block[block_id].push_back(k);
	}
}

int main(int argc, char* argv[]) 
{
	static struct option long_options[] =
	{ 
		{"nodenum", 1, 0, 0}, // 0
		{"chunk", 1, 0, 0}, // 1
		{"threads", 1, 0, 0}, // 1
	};

	// set parameters.
	while(1)
	{
		int longindex = -1;
		int c = getopt_long_only(argc, argv, "", long_options, &longindex);
		if(c == -1) break; // Done parsing flags.
		else if(c == '?'){ // If the user entered junk, let him know. 
			cerr << "Invalid parameters." << endl;
			//usage(argv[0]);
		}else {
			// Branch on long options.
			switch(longindex) 
			{ 
				case 0: nodenum = atoi(optarg); break;
				case 1: chunk = atoi(optarg); break;
				case 2: thread_num = atoi(optarg); break;

				default: break; 
			}
		}
	}

	int NodeNum = nodenum-1;
	chunk_size = chunk*BASE;
	//chunk_size = chunk;

	// get Genome file name.
	string ref_fasta = argv[optind]; 
	// Load total genomes part information.
	loadPartInfoOfTotalGenomes(ref_fasta, totalgenomes);

	// sort genomes by length.
	sortByGenomeLength(totalgenomes);
	renumberTotalgenomesId(totalgenomes);

	//string output_totalgenomes_sort = output_totalgenomes + ".sorted";
	string output_totalgenomes_sort = ref_fasta + ".tg";

	makeChunk(chunkinfo, MAXChunkSize, MAXSeqNum, totalgenomes, chunk_size, thread_num);
	cerr<<"# MAXChunksize = "<< MAXChunkSize <<",  MAXSeqNumOfChunk = "<< MAXSeqNum <<endl;

	cerr<<"Result file name $"<<output_totalgenomes_sort<<"$, after sort."<<endl;
	cerr<<"Output genomes information ......\n"<<endl;
	outputPartInfoOfTotalGenomes(output_totalgenomes_sort, totalgenomes);

	outputChunkInfo(ref_fasta, chunkinfo, NodeNum);

	makeBlock(block, chunkinfo, NodeNum);
	outputBlockInfo(ref_fasta, block, chunkinfo);


	cerr<<"\n==========================="<<endl; 
	cerr<<"The finish.\n"<<endl;
	return 0; // The end.
}

void usage(string prog) 
{
	cerr << "sort is a sort and file dividing program for genome file .fna:"<<endl;
	cerr << endl;
	cerr << "Usage: " << prog << " [options] <genomes-file> " << endl;
	cerr << endl;
	cerr << "Options:" << endl;
	cerr << endl;
	cerr << "-nodenum       the value of the number of node >= 2, including server node and worker node, the default value is 2" << endl;
	cerr << "-chunk         chunk size for one time clustering, the default value is 100 (MB), the value must be greater than the size of each single sequence" << endl;
	cerr << "-threads       number of threads used in gclust, the default value is 1 " << endl;
	cerr << endl;
	cerr << "Example usage:" << endl;
	cerr << endl;
	cerr << "./sort -nodenum 3 -chunk 2 -threads 2 test.fna" << endl;
	cerr <<endl;
	cerr << "" <<endl;
	cerr << endl;
	exit(1);

}
