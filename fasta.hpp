#ifndef __FASTA_HPP__
#define __FASTA_HPP__
#include <string>
#include <vector>
#include <sstream>
using namespace std;
// Clustering information.
struct hit
{
	long id;
	double identity;
	char strand;
};

// For single genome search.
struct Genome
{
	long size; //genome length
	long id; //global index
	string descript; // genome name
	string cont; // genome content a string
	
	long chunk_id;// chunk id for mpi base version

};

// Total genomes info container.
struct GenomeClustInfo
{
	long size; //genome length
	long id; //index
	bool rep; // if it is representive of clustering
	string descript; //genome name
	vector<hit> clusters, clusterunits;

	long chunk_id;// chunk id for mpi base version
};

string int2str(int a);
//functions 
void trim(string &line, long &start, long &end);
void filter_n(string &seq_rc);
void reverse_complement(string &seq_rc, bool nucleotides_only);

void make_block_ref(vector<Genome> &partgenomes,
		vector<GenomeClustInfo> &totalgenomes,
		string &S,
		int *&mask_tg,
		vector<long> &descr,
		vector<long> &startpos);

void getClusteringInfoOnepart(vector<GenomeClustInfo> &totalgenomes,
		vector<Genome> &qrygenomes,
		long begin,
		long chunk,
		bool &clusterhit);
//load fasta data just one step ahead of bcasting
void load_fasta_master(string filename, vector<GenomeClustInfo> &totalgenomes);

void load_fasta_worker_all(string filename, vector<Genome> &allgenomes);
void load_fasta_worker_part(string filename, vector<Genome> &partgenomes, long start_id, long &number);

//load total genomes to memory for each node	
void load_total_genomes(string filename, 
						vector<GenomeClustInfo> &totalgenomes, 
						long &total_chunks, 
						long &max_seq_num, 
						long &max_chunk_size,
						vector<long> &chunk_ptr);

void load_one_chunk(string filename, vector<Genome> &chunk_genomes,
					long chunk_id, int worker_size);
//support different kinds of search algorithms
void convert_to_cdhit();
void convert_to_gclust();
void convert_to_usearch();
void convert_to_dnaclust();

#endif // __FASTA_HPP__
